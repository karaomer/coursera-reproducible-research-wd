# Storm Damage Analysis of USA - 1950 to 2011
Omer Kara  
Tuesday, August 05, 2014  

## Synopsis
Extreme weather events can be harmful and costly for communities through causing fatalities, injuries, property and crop damage. This analysis uses the [U.S. National Oceanic and Atmospheric Administration's (NOAA) Storm Database](http://www.ncdc.noaa.gov/stormevents/), characterizes major storms and weather events in the Unitied States from 1950 to 2011, to explore which types of events (1) are most harmful with respect to population health and (2) have the greatest economic consequences. 

Analysis of the top 40000 most costly events shows that, economically, **Hurricane** is the most costly weather event in terms of total cost and property damage cost. **Heat Related** weather event is the most harmful one on crop damage.

Analysis of the events with at least one harmful result shows that, in terms of public health, **Tornado** is the most harmful weather event in terms of both injuries and fatalities; and thus, in total harm as well.


## Data Processing


####**1.** The Raw Data
Date Accessed: 8/11/2014 6:52:32 AM  

The data of this analysis come from the [U.S. National Oceanic and Atmospheric Administration's (NOAA) Storm Database](http://www.ncdc.noaa.gov/stormevents/). This database tracks characteristics of major storms and weather events in the United States, including when and where they occur, as well as estimates of any fatalities, injuries, and property damage.

The data for this analysis come in the form of a comma-separated-value file compressed via the `bzip2 algorithm` to reduce its size.  
You may directly download the file from [Storm Data](https://d396qusza40orc.cloudfront.net/repdata%2Fdata%2FStormData.csv.bz2) [47Mb] or you may use *The Reproducible Way to Download/Unzip The Data File* section in this document.

There is also some documentation of the database available from the original source. Here you will find how of the variables are constructed/defined.  
- National Weather Service [Storm Data Documentation](https://d396qusza40orc.cloudfront.net/repdata%2Fpeer2_doc%2Fpd01016005curr.pdf)  
- National Climatic Data Center Storm Events [FAQ](https://d396qusza40orc.cloudfront.net/repdata%2Fpeer2_doc%2FNCDC%20Storm%20Events-FAQ%20Page.pdf)  

The events in the database start in the year 1950 and end in November 2011. In the earlier years of the database there are generally fewer events recorded, most likely due to a lack of good records. More recent years should be considered more complete.


####**2.** The Reproducible Way to Download The Data File.
    Note: If the zipped data file is already in the current WD, you may skip this step.

Since the file compressed via the `bzip2 algorithm`, the size of unzipped data file is large (535Mb). Thus, it is better to read the data directly from the zipped file.  

The following code chunk offers you a reproducible way to check if the zipped data file exists in your current WD, if not, it creates a folder called `./data` in the current WD and sets WD to this folder, and finally downloads the zipped data file into `./data`.

Since preprocessing data is time-consuming, `cache = TRUE` is set in the preprocessing data code chunks, as well as some other time consuming chunks, so you can avoid downloading the 49MB file and read the data every time you `knit` your `.Rmd` file.

```r
fileNameZip <- "repdata-data-StormData.csv.bz2"
fileDestZip <- paste("./", fileNameZip, sep = "")
fileURL <- "http://d396qusza40orc.cloudfront.net/repdata%2Fdata%2FStormData.csv.bz2" ## Be sure to use http, instead of https in the file URL since it gives errors in .Rmd files.

if (file.exists(fileNameZip)) {
    message("Data file already exists in the current WD")
    } else {
        message("Download the zipped data file into './data' directory")
        if (!file.exists("./data")) {
            dir.create("./data")
            }
        setwd("./data")
        download.file(fileURL, fileDestZip, method = "auto")
        }
```


####**3.** Read The Data
`read.csv` function can read the bzip2 compressed files without having to unzip them. However, it takes much longer time compared to reading unzipped files directly. So, please be patient while the data is being read.
To shorten data reading time, we can use options in `read.csv` by specifying them before reading the whole data.  

* `colClasses`: Helps us to determine the column classes by reading a very small part of the data. Then we can use determined column classes to shorten the whole data reading time.  
* `nrows`: Specifying the 'nrows' argument doesn't necessary make things go faster but it can help a lot with memory usage. With the suggestions of Prof. Peng in [Reading large tables into R](http://www.biostat.jhsph.edu/~rpeng/docs/R-large-tables.html) and Pradeep Mavuluri in [Coastal Econometrician Views](http://costaleconomist.blogspot.com/2010/02/easy-way-of-determining-number-of.html), row numbers of the file can be determined, before actually loading its data.  
* `comment.char`: setting to = "" can decrease reading time which is a default option in `read.csv`.

```r
classes <- sapply(read.csv("./repdata-data-StormData.csv.bz2", nrows = 100),
                  class)
con <- file("./repdata-data-StormData.csv.bz2", open = "r") ## Check inside of the file before loading
readsizeof <- 20000
nooflines <- 0
(while((linesread <- length(readLines(con, readsizeof))) > 0 ) ## while loop to count nrows
nooflines <- nooflines + linesread ) ## will expectedly return "## NULL" when complete
close(con)
nooflines
```

Unfortunately, even though the column classes are exactly defined from R in `classes` object, R expects the column classes should be different/mis-specified and thus returns error in data reading. Also, `nrows` approach does not seem to shorten data reading time substantionally. Therefore, `read.csv` function is simply used without any options specified. 

```r
storm.data <- read.csv("./repdata-data-StormData.csv.bz2")
dim(storm.data)
```

```
## [1] 902297     37
```


####**4.** Subset/Transform/Clean The Data
Since, the ultimate goal of this analysis is to address the impact of *general types of events* on population health and economic consequences, it better to subset the raw data with necessary variables for computational purposes. The necessary varibles and their explanations with tranforming/cleaning codes are below.

```r
data <- storm.data[, c("EVTYPE", "FATALITIES", "INJURIES", "PROPDMG", 
                       "PROPDMGEXP", "CROPDMG", "CROPDMGEXP")]
sapply(data, function(missing) any(is.na(missing))) ## Check missing values.
```

```
##     EVTYPE FATALITIES   INJURIES    PROPDMG PROPDMGEXP    CROPDMG 
##      FALSE      FALSE      FALSE      FALSE      FALSE      FALSE 
## CROPDMGEXP 
##      FALSE
```

* FATALITIES: Number of fatalities (i.e., deaths) associated with the event. Comes from the raw data.
* INJURIES: Number of injuries associated with the event. Comes from the raw data.
* totalharm: Sum of fatalities and injuries associated with the event. 

```r
data$totalharm <- data$FATALITIES + data$INJURIES
```

* PROPDMG: Property damange amount.
* CROPDMG: Crop damange amount.
* PROPDMGEXP: Unit of property damage. Alphabetical characters used to signify magnitude include "K" for thousands, "M" for millions, "B" for billions and "" for non. If additional precision is available, it may be provided in the narrative part of the entry.
* CROPDMGEXP: Unit of crop damange. Same logical alphabetical units used in `PROPDMGEXP` applies.
    * According to the original data documentation there ar 4 units for `PROPDMG` and `CROPDMG`; however, computations clearly shows that there are some other alphabetical units such as +, -, ?, h and numbers (instead of letter). So, this analysis interprests signs as non units, and h as hundreds. 

```r
unique(data$PROPDMGEXP)
```

```
##  [1] K M   B m + 0 5 6 ? 4 2 3 h 7 H - 1 8
## Levels:  - ? + 0 1 2 3 4 5 6 7 8 B h H K m M
```

```r
unique(data$CROPDMGEXP)
```

```
## [1]   M K m B ? 0 k 2
## Levels:  ? 0 2 B k K m M
```


```r
data$PROPDMGEXP <- chartr("-?+hHkKmMbB", "11122336699", data$PROPDMGEXP)
data$PROPDMGEXP[data$PROPDMGEXP == ""] <- 0
data$PROPDMGEXP <- as.numeric(data$PROPDMGEXP)

data$CROPDMGEXP <- chartr("-?+hHkKmMbB", "11122336699", data$CROPDMGEXP)
data$CROPDMGEXP[data$CROPDMGEXP == ""] <- 0
data$CROPDMGEXP <- as.numeric(data$CROPDMGEXP)

data$propdamage <- data$PROPDMG * (10^(data$PROPDMGEXP-6)) ## PROPDMG in million $
data$cropdamage <- data$CROPDMG * (10^(data$CROPDMGEXP-6)) ## CROPDMG in million $
data$totaldamage <- data$propdamage + data$cropdamage ## Total damage in million $

sort(data$totaldamage, decreasing = TRUE)[1:5] ## Check whether everthing is ok. It seems there is something wrong either in code or raw data, since the max value of damage is 115 billion $.
```

```
## [1] 115033  31300  16930  11260  10000
```

```r
storm.data[data$totaldamage == max(data$totaldamage), "REMARKS" ] ## Check the data description. Seems that raw data should be in millions not in billions.
```

```
## [1] Major flooding continued into the early hours of January 1st, before the Napa River finally fell below flood stage and the water receeded. Flooding was severe in Downtown Napa from the Napa Creek and the City and Parks Department was hit with $6 million in damage alone. The City of Napa had 600 homes with moderate damage, 150 damaged businesses with costs of at least $70 million.
## 436781 Levels:  -2 at Deer Park\n ... Zones 22 and 23 were added to the high wind warning of  January 26. Peak winds Sitka 55MPH, Cape Decision 58MPH, and Cape Spencer 64MPH.\n
```

```r
# Correction and recalculation
data[data$totaldamage == max(data$totaldamage), "PROPDMGEXP"] <- 6
data$propdamage <- data$PROPDMG * (10^(data$PROPDMGEXP-6)) ## PROPDMG in million $
data$cropdamage <- data$CROPDMG * (10^(data$CROPDMGEXP-6)) ## CROPDMG in million $
data$totaldamage <- data$propdamage + data$cropdamage ## Total damage in million $
```

After subsetting and transforming important variables, one final step before dealing with troublesome EVTYPE variable. Here, the analysis subsets the data for necessary variables and lowers the column names. 

```r
data <- data[, c(-4, -5, -6, -7)] ## Cleaning the unused vairables
colnames(data) <- tolower(names(data)) ## Lowering all the variable names to make the data tidy.
```

* evtype: The hazardous event type. The original data documentation lists 48 main event types and several subtypes. Please see [Storm Data Documentation](https://d396qusza40orc.cloudfront.net/repdata%2Fpeer2_doc%2Fpd01016005curr.pdf) pages 2-4. Note that these 48 main event types are catogorized very specificlly such as 
"Marine Thunderstorm Wind" and "Thunderstorm Wind". Without using a simplfying approach in event types, we will end up with 985 unique event types which is not computationally feasible. 

```r
length(unique(storm.data$EVTYPE))
```

```
## [1] 985
```

Since the goal of this analysis is to address the impact of *general type of events* on population health and economic consequences, the analysis will constructs its own *general type of events* and subsets the data for population health analysis and economic consequences analysis separately. This approach will not harm the ultimate goal of the analysis but simplfies the analysis.


#####**4.1.** Subsetting `evtype` for Economic Consequences Analysis
The top 40000 most costly (total damage) events out of 902297 total events represents %99 of the total damage for all events. So, to simplfy the work on dealing with evtype variable, this analysis subsets the data with the top 40000 most costly events. However the unique number of events are still high (217) for compuattional analysis.

```r
percent.totaldamage <- sum(sort(data$totaldamage, decreasing = T)[1:40000]) / sum(data$totaldamage)
percent.totaldamage
```

```
## [1] 0.9909
```

```r
economic <- data[order(data$totaldamage, decreasing = TRUE)[1:40000], c(-2, -3, -4)]
dim(economic)
```

```
## [1] 40000     4
```

```r
length(unique(economic$evtype)) ## Number of event types are reduced to 217.
```

```
## [1] 217
```

217 unique event types are categorized into generally equivalent event types and the unique event types are reduced to 13.

```r
economic$evtype <- as.character(economic$evtype)

# Regular expressions for generally equivalent event types. 
keywords1 <- c("(.*FLOOD.*|.*fld.*)", "(.*TORN.*|.*WATERSPOUT.*)", "(.*HURRICANE.*|.*TYPHOON.*)", "(.*THUNDE.*|.*STORM.*|.*TROPICAL DEPRESSION.*)", ".*FIRE.*", "(.*ICE.*|.*FROST.*|.*SNOW.*|.*HAIL.*|.*FREEZ.*|.*BLIZ.*|.*WINT.*|.*GLAZE.*|.*COLD.*)", ".*WIND.*", "(.*RAIN.*|.*PRECIPITATION.*|.*WET.*|.*HEAVY MIX.*)", "(.*HEAT.*|.*DRY.*|.*DROUGHT.*)", ".*FOG.*", "(.*TIDE.*|.*SURF.*|.*SEICHE.*|.*TSUNAMI.*|.*COASTAL SURGE.*|.*HIGH WATER.*)", "(.*AVALANCHE.*|.*EROSION.*|.*SLIDE.*|.*Landslump.*)", "(.*DAM BREAK.*|.*LIGHTNING.*|.*VOLCANIC ASH.*)")

# Replace with the following general event types. 
replace1 <- c("FLOOD", "TORNADO", "HURRICANE", "STORM", "FIRE", "WINTER RELATED", "WIND", "RAIN", "HEAT RELATED", "FOG", "OCEAN RELATED", "LAND RELATED", "OTHER")

# Table of regular expressions and generally equivalent event types.
library(xtable)
table1 <- xtable(cbind(keywords1, replace1))
print(table1, type = "html")
```

<!-- html table generated in R 3.1.1 by xtable 1.7-3 package -->
<!-- Tue Aug 05 19:22:47 2014 -->
<TABLE border=1>
<TR> <TH>  </TH> <TH> keywords1 </TH> <TH> replace1 </TH>  </TR>
  <TR> <TD align="right"> 1 </TD> <TD> (.*FLOOD.*|.*fld.*) </TD> <TD> FLOOD </TD> </TR>
  <TR> <TD align="right"> 2 </TD> <TD> (.*TORN.*|.*WATERSPOUT.*) </TD> <TD> TORNADO </TD> </TR>
  <TR> <TD align="right"> 3 </TD> <TD> (.*HURRICANE.*|.*TYPHOON.*) </TD> <TD> HURRICANE </TD> </TR>
  <TR> <TD align="right"> 4 </TD> <TD> (.*THUNDE.*|.*STORM.*|.*TROPICAL DEPRESSION.*) </TD> <TD> STORM </TD> </TR>
  <TR> <TD align="right"> 5 </TD> <TD> .*FIRE.* </TD> <TD> FIRE </TD> </TR>
  <TR> <TD align="right"> 6 </TD> <TD> (.*ICE.*|.*FROST.*|.*SNOW.*|.*HAIL.*|.*FREEZ.*|.*BLIZ.*|.*WINT.*|.*GLAZE.*|.*COLD.*) </TD> <TD> WINTER RELATED </TD> </TR>
  <TR> <TD align="right"> 7 </TD> <TD> .*WIND.* </TD> <TD> WIND </TD> </TR>
  <TR> <TD align="right"> 8 </TD> <TD> (.*RAIN.*|.*PRECIPITATION.*|.*WET.*|.*HEAVY MIX.*) </TD> <TD> RAIN </TD> </TR>
  <TR> <TD align="right"> 9 </TD> <TD> (.*HEAT.*|.*DRY.*|.*DROUGHT.*) </TD> <TD> HEAT RELATED </TD> </TR>
  <TR> <TD align="right"> 10 </TD> <TD> .*FOG.* </TD> <TD> FOG </TD> </TR>
  <TR> <TD align="right"> 11 </TD> <TD> (.*TIDE.*|.*SURF.*|.*SEICHE.*|.*TSUNAMI.*|.*COASTAL SURGE.*|.*HIGH WATER.*) </TD> <TD> OCEAN RELATED </TD> </TR>
  <TR> <TD align="right"> 12 </TD> <TD> (.*AVALANCHE.*|.*EROSION.*|.*SLIDE.*|.*Landslump.*) </TD> <TD> LAND RELATED </TD> </TR>
  <TR> <TD align="right"> 13 </TD> <TD> (.*DAM BREAK.*|.*LIGHTNING.*|.*VOLCANIC ASH.*) </TD> <TD> OTHER </TD> </TR>
   </TABLE>
**Table 1**: able of regular expressions and generally equivalent event types for the top 40000 most costly events.


```r
# For loop to replace the regular expressions with general event types.  
for(index in 1:length(keywords1)) {
    obs <- grep(keywords1[index], economic$evtype, ignore.case = TRUE)
    if(length(obs) > 0) {
        economic[obs, "evtype"] <- replace1[index]
        }
    }
economic$evtype <- as.factor(economic$evtype)
length(unique(economic$evtype))
```

```
## [1] 13
```

Melting the `economic` data set by damage type and aggregating propdamage, cropdamage and totaldamage across generally equivalent event types. `economic.sum` is the final data set used for explatory data analysis for the impact of event types on economic consequences.

```r
library(reshape2)
economic.sum <- aggregate(.~ evtype + variable, data = melt(economic, id.vars = c("evtype")), FUN = sum)
```


#####**4.2.** Subsetting `evtype` for Public Health Harm Analysis
Approach in selecting evtype for public health analysis is quite different from the economic consequences analysis since the many events do not report fatalities or injuries. Thus, subsetting approach is selecting events that have at least one injury or one fatalitiy. However the unique number of events are still high (220) for compuattional analysis.

```r
harm <- data[(data$injuries > 0 | data$fatalities > 0), 1:4]
dim(harm)
```

```
## [1] 21929     4
```

```r
length(unique(harm$evtype)) ## Number of event types are reduced to 220.
```

```
## [1] 220
```

220 unique event types are categorized into generally equivalent event types and the unique event types are reduced to 14.

```r
harm$evtype <- as.character(harm$evtype)

# Regular expressions for generally equivalent event types. 
keywords2 <- c("(.*FLOOD.*|.*fld.*)", "(.*TORN.*|.*WATERSPOUT.*|.*DUST DEVIL.*|.*FUNNEL CLOUD.*)", "(.*HURRICANE.*|.*TYPHOON.*)", "(.*THUNDE.*|.*STORM.*|.*TROPICAL DEPRESSION.*)", ".*FIRE.*", "(.*ICE.*|.*FROST.*|.*SNOW.*|.*HAIL.*|.*FREEZ.*|.*BLIZ.*|.*WINT.*|.*GLAZE.*|.*COLD.*|.*ICY.*|.*HYPOTHERMIA.*|.*SLEET.*|.*LOW TEMP.*)", ".*WIND.*", "(.*RAIN.*|.*PRECIP.*|.*WET.*|.*HEAVY MIX.*)", "(.*HEAT.*|.*DRY.*|.*DROUGHT.*|.*HYPERTHERMIA.*|.*WARM WEATHER.*|.*UNSEASONABLY WARM.*)", ".*FOG.*", "(.*TIDE.*|.*SURF.*|.*SEICHE.*|.*TSUNAMI.*|.*COASTAL SURGE.*|.*HIGH WATER.*|.*SEAS.*|.*HIGH SWELLS.*|.*WAVE.*|.*CURRENT.*|.*RAPIDLY RISING WATER.*)", "(.*AVALAN.*|.*EROSION.*|.*SLIDE.*|.*Landslump.*)", ".*LIGHTNIN.*", "(.*DROWNING.*|HIGH|OTHER|.*Accident.*|.*MISHAP.*)")

# Replace with the following general event types. 
replace2 <- c("FLOOD", "TORNADO", "HURRICANE", "STORM", "FIRE", "WINTER RELATED", "WIND", "RAIN", "HEAT RELATED", "FOG", "OCEAN RELATED", "LAND RELATED", "LIGHTNING", "OTHER")

# Table of regular expressions and generally equivalent event types.
library(xtable)
table2 <- xtable(cbind(keywords2, replace2))
print(table2, type = "html")
```

<!-- html table generated in R 3.1.1 by xtable 1.7-3 package -->
<!-- Tue Aug 05 19:22:53 2014 -->
<TABLE border=1>
<TR> <TH>  </TH> <TH> keywords2 </TH> <TH> replace2 </TH>  </TR>
  <TR> <TD align="right"> 1 </TD> <TD> (.*FLOOD.*|.*fld.*) </TD> <TD> FLOOD </TD> </TR>
  <TR> <TD align="right"> 2 </TD> <TD> (.*TORN.*|.*WATERSPOUT.*|.*DUST DEVIL.*|.*FUNNEL CLOUD.*) </TD> <TD> TORNADO </TD> </TR>
  <TR> <TD align="right"> 3 </TD> <TD> (.*HURRICANE.*|.*TYPHOON.*) </TD> <TD> HURRICANE </TD> </TR>
  <TR> <TD align="right"> 4 </TD> <TD> (.*THUNDE.*|.*STORM.*|.*TROPICAL DEPRESSION.*) </TD> <TD> STORM </TD> </TR>
  <TR> <TD align="right"> 5 </TD> <TD> .*FIRE.* </TD> <TD> FIRE </TD> </TR>
  <TR> <TD align="right"> 6 </TD> <TD> (.*ICE.*|.*FROST.*|.*SNOW.*|.*HAIL.*|.*FREEZ.*|.*BLIZ.*|.*WINT.*|.*GLAZE.*|.*COLD.*|.*ICY.*|.*HYPOTHERMIA.*|.*SLEET.*|.*LOW TEMP.*) </TD> <TD> WINTER RELATED </TD> </TR>
  <TR> <TD align="right"> 7 </TD> <TD> .*WIND.* </TD> <TD> WIND </TD> </TR>
  <TR> <TD align="right"> 8 </TD> <TD> (.*RAIN.*|.*PRECIP.*|.*WET.*|.*HEAVY MIX.*) </TD> <TD> RAIN </TD> </TR>
  <TR> <TD align="right"> 9 </TD> <TD> (.*HEAT.*|.*DRY.*|.*DROUGHT.*|.*HYPERTHERMIA.*|.*WARM WEATHER.*|.*UNSEASONABLY WARM.*) </TD> <TD> HEAT RELATED </TD> </TR>
  <TR> <TD align="right"> 10 </TD> <TD> .*FOG.* </TD> <TD> FOG </TD> </TR>
  <TR> <TD align="right"> 11 </TD> <TD> (.*TIDE.*|.*SURF.*|.*SEICHE.*|.*TSUNAMI.*|.*COASTAL SURGE.*|.*HIGH WATER.*|.*SEAS.*|.*HIGH SWELLS.*|.*WAVE.*|.*CURRENT.*|.*RAPIDLY RISING WATER.*) </TD> <TD> OCEAN RELATED </TD> </TR>
  <TR> <TD align="right"> 12 </TD> <TD> (.*AVALAN.*|.*EROSION.*|.*SLIDE.*|.*Landslump.*) </TD> <TD> LAND RELATED </TD> </TR>
  <TR> <TD align="right"> 13 </TD> <TD> .*LIGHTNIN.* </TD> <TD> LIGHTNING </TD> </TR>
  <TR> <TD align="right"> 14 </TD> <TD> (.*DROWNING.*|HIGH|OTHER|.*Accident.*|.*MISHAP.*) </TD> <TD> OTHER </TD> </TR>
   </TABLE>
**Table 2**: Regular expressions and generally equivalent event types for the top 40000 most costly events.


```r
# For loop to replace the regular expressions with general event types.  
for(index in 1:length(keywords2)) {
    obs <- grep(keywords2[index], harm$evtype, ignore.case = TRUE)
    if(length(obs) > 0) {
        harm[obs, "evtype"] <- replace2[index]
        }
    }
harm$evtype <- as.factor(harm$evtype)
length(unique(harm$evtype))
```

```
## [1] 14
```

Melting the `economic` data set by damage type and aggregating fatalities, injuries and totalharm across generally equivalent event types. `harm.sum` is the final data set used for explatory data analysis for the impact of event types on public health harm.

```r
harm.sum <- aggregate(.~ evtype + variable, data = melt(harm, id.vars = c("evtype")), FUN = sum)
```


####**5.** Results
#####**5.2.** Economic Consequences Analysis
*The Economic Consequences by Generally Equivalent Event Types* is shown below in Figure 1. Economically, Hurricane is the most costly weather event in terms of total cost and property damage cost. Heat Related weather event is the most harmful one on crops.

```r
economic.index <- as.character(economic.sum[27:39,][order(economic.sum[27:39, ]$value, decreasing = TRUE), "evtype"]) ## Creating index for the most costly event types to use in graph.

library(ggplot2)
ggplot(data = economic.sum[1:26,], aes(evtype, value, fill = variable)) + geom_bar(stat = "identity") + coord_flip() + xlim(rev(economic.index)) + labs(title = "Economic Consequences by Generally Equivalent Events", y = "Total Cost in Million $", x = "Generally Equivalent Events Types") + scale_fill_manual(values = c("lightblue3", "lightblue4"), name = "Damage Type", breaks = c("propdamage", "cropdamage"), labels = c("Property Damage", "Crop Damage"))+ theme(text = element_text(size = 12))
```

![plot of chunk economicResults](./Assignment2_files/figure-html/economicResults.png) 

**Figure 1**: Top 40000 most costly events by generally equivalent event type.


#####**5.2.** Public Health Harm Analysis
*The Public Health Harm by Generally Equivalent Event Types* is shown below in Figure 2. In terms of Public Health, Tornado is the most harmful weather event in terms of both injuries and fatalities; and thus, in total harm as well.

```r
harm.index <- as.character(harm.sum[29:42,][order(harm.sum[29:42, ]$value, decreasing = TRUE), "evtype"]) ## Creating index for the most harmful event types to use in graph.

ggplot(data = harm.sum[28:1,], aes(evtype, value, fill = variable)) + geom_bar(stat = "identity") + coord_flip() + xlim(rev(harm.index)) + labs(title = "Harm on Public Health by Generally Equivalent Event Types", y = "Total Harm Puplic Health", x = "Generally Equivalent Events Types") + scale_fill_manual(values = c("lightblue4", "lightblue3"), name = "Harm Type", breaks = c("fatalities", "injuries"), labels = c("Fatalities", "Injuries"))+ theme(text = element_text(size = 12))
```

![plot of chunk harmResults](./Assignment2_files/figure-html/harmResults.png) 

**Figure 2**: Injuries and Fatalities by generally equivalent event type.





